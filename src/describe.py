import pandas as pd 
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import numpy as np

def describe(data):
    return data[data.columns[0]].describe()

def heatmap_features(df):
    
    corrmat = df.corr()
    corrmat1 = _filter_by_correlation(corrmat,0.5)
    corrmat2 = _filter_by_correlation(corrmat,0.25)
    fig, axn = plt.subplots(1, 1, sharex=True, sharey=True,figsize=(12,12))
    sns.set(font_scale=0.5)
    ax1 = axn
    #ax2 = axn.flat[1]
    sns.heatmap(corrmat1, ax=ax1, vmax = .8, square=True, annot=True,fmt='.2f', cmap='summer', cbar=False)
    #sns.heatmap(corrmat2, ax=ax2, vmax=.8, square=True, annot=True, fmt='.2f', cmap='summer')

    plt.show()

def _filter_by_correlation(df, corr):
    new_df = df.copy(deep=True)
    for column in new_df:
        # to_drop = df.iloc[v] for v in new_df[column][((new_df[column] ==1) | (abs(new_df[column]) < corr )) ]
        print(to_drop)
        new_df[column].drop(inplace=True ,labels=to_drop)
    return new_df

def heatmap_top_10(df):
    corrmat = df.corr()
    k = 10 #number of variables for heatmap
    cols = corrmat.nlargest(k, 'SalePrice')['SalePrice'].index
    cm = np.corrcoef(df[cols].values.T)
    sns.set(font_scale=1.25)
    hm = sns.heatmap(cm, cbar=True, annot=True, square=True, fmt='.2f', annot_kws={'size': 10}, yticklabels=cols.values, xticklabels=cols.values, cmap='winter')
    plt.show()

def scatter_plot_multiple_variables(df):
    sns.set()
    cols = ['SalePrice', 'OverallQual', 'GrLivArea', 'GarageCars', 'TotalBsmtSF', 'FullBath', 'YearBuilt']
    sns.pairplot(df[cols], size = 2.5)
    plt.show()

def describe_missing_data(df):
    total = df.isnull().sum().sort_values(ascending=False)
    percent = (df.isnull().sum()/df.isnull().count()).sort_values(ascending=False)
    missing_data = pd.concat([total, percent], axis=1, keys=['Total', 'Percent'])
    print(missing_data.head(20))

def find_columns_with_most_missing_data(df):
    actual = df.isnull().sum()
    total = df.isnull().count()
    missing = pd.concat([actual, total], axis=1,keys=['Actual','Total'])
    return  missing.sort_values('Actual', ascending=False)