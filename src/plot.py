from matplotlib import pyplot as plt
import seaborn as sns
from .describe import *

def show_top_missing_values(df, top=10):
    f, ax = plt.subplots(figsize=(7, 9))
    df = find_columns_with_most_missing_data(df)
    df = df[0:top]
    sns.barplot(x=df.index, y=df["Actual"])
    plt.xlabel('Features', fontsize=15)
    plt.ylabel('Percent of missing values', fontsize=15)
    plt.title('Percent missing data by feature', fontsize=15)
    plt.xticks(rotation='90')
    plt.show()