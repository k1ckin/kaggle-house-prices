
def check_for_missing_in_dataframe(df):
    for c in df:
        if (check_for_missing_in_series(df[c]) == False):
            return False
    return True
    
def check_for_missing_in_series(series):
    return not series.isnull().any()