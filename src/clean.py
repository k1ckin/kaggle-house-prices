import pandas as pd

def remove_column_from_both_datasets(tuple, columns):
    for df in tuple:
        df.drop(columns=columns, inplace=True)
    return tuple

def remove_rows_for_columns_where_missing_data(df, columns):
    for c in columns:
        null_series = df[c].isnull()
        indices = null_series[null_series == True ].index
        before = len(df)
        df.drop(indices,inplace=True)
        after = len(df)
    return df

def impute_dataframe_with_mean(df):
    return df.fillna(df.mean())

def impute_categorical_series_with(df, array, imputer_string="Uknown"):
    for category in array:
        df[category].fillna(imputer_string, inplace=True)
    return df