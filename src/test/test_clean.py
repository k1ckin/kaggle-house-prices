import unittest
from ..clean import *
from ..verify import *
import pandas as pd
import numpy as np

class TestDataCheckers(unittest.TestCase):

    def test_check_for_missing_should_fail(self):
        d = dict( A = np.array([1,2]), B = np.array([1,2,3,4]) )
        failing = pd.DataFrame(dict([ (k,pd.Series(v)) for k,v in d.items() ]))
        self.assertIs(check_for_missing_in_dataframe(failing), False)
    
    def test_check_for_missing_should_pass(self):
        passing = pd.DataFrame({"a": [2,3,4], "b": [2,3,6]})
        self.assertIs(check_for_missing_in_dataframe(passing), True)

