# Kaggle House Price Challenge
Objective: find the price of a house based on features (detailed in data/Definitiions.txt)

## Steps taken:
- Clean data
- - Removed record without 'Electrical' feature
- - Removed columns 'PoolQC','MiscFeature','Alley'






## As per this kernel(by https://www.kaggle.com/pmarcelino/comprehensive-data-exploration-with-python)
- Go through features and contextually (through common sense) pick 3-4 'essential features' to test against 
- Describe the output variable/prediction. Plot is and see the distribution
- Plot each essential feature to the label
- - For continous data - scatter plot 
- - For ordinal data - boxplot 
- Plop top 10 correlations on heatmap, remember the feature names
- Plot those features (scatter_plot_multiple_variables)
- Missing data - how common and *is it random*? (describe_missing_data)
- - Delete features with more than 15% missing data?
- - Delete features that have less missing data but appear uncorrelated
- - For columns with 1 missing record, just delete that record 
- Outliers 
- - Univariate analysis
- - - Standardize output columns (mean=0, stdev=1)
- - - Check skewness
- - Bivariate analysis
- - - Plot essential variables against prediction columns
- - - Notice trend and see if small number of outliers exists that are faaar of
- - - Delete those
- Getting hard core (point 5). Checkout step/GettingHardCore
- - Testing 4 assumptions
- - - Normality - is the data normally distributed?
- - - - If not and if left skewed - apply np.log(Column) to make it normally distributed
- - - Homoscedasticity - the assumption that dependent variables exhibit equal levels of variance across the range of predictor variables
- - - Linearity - look for linear relationships
- - - Absence of correlated errors - if change in one positive error makes a negative/positive error in another



## Assumed steps
Probably first use PCA to reduce # of dimensions. 

### Questions to answer
- [Cleaning] How do they determine which features to keep? In other words - do we keep all regardless and if not - what is the criteria?
- [Cleaning] Do we remove any outliers?
- [Transformation] Do they one-hot encode categorical data? If so - according to what criteria?
- [Transformation] Do they scale down (standardize) data? If so - do they standardize all continous data?

