from src.data_in import raw_test, raw_train
from src.describe import *
from src.plot import *
from src.clean import *
from src.verify import *

columns_to_remove = ['PoolQC','MiscFeature','Alley',
 'Fence' ,
  'FireplaceQu', #670 missing,
 'LotFrontage', #250,
 ]
(test,train) = remove_column_from_both_datasets((raw_test,raw_train) , columns_to_remove)

columns_for_rows_to_remove_where_null = ['Electrical']
train = remove_rows_for_columns_where_missing_data(train, columns_for_rows_to_remove_where_null)


impute_unknown = ['BsmtCond','BsmtQual','MasVnrType', 'MasVnrArea', 'BsmtFinType2', "GarageType","GarageYrBlt","GarageFinish","GarageQual", "GarageCond",'BsmtExposure','BsmtFinType1']
train = impute_categorical_series_with(train, impute_unknown)




# TODO Impute the rest of values 

if (not check_for_missing_in_dataframe(train)): raise Exception("Missing values, cancelling")

# Done Cleaning
#

#
# Begin Transforming

print('------------')
print("Data is clean now!")